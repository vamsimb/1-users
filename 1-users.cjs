const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}





// Q1 Find all users who are interested in playing video games.


// console.log("..........................Test1..........................")


const usersPlayingVideoGames = Object.fromEntries(Object.entries(users)

    .filter(user => String(user[1]['interests']).includes('Video Games') || String(user[1]['interest']).includes('Video Games')));


console.log(usersPlayingVideoGames);


// Q2 Find all users staying in Germany.

// console.log("..........................Test2..........................")


const usersInGermany = Object.keys(users).filter(user => users[user].nationality == "Germany");
console.log(usersInGermany)

// console.log("..........................Test3..........................")

// // Q3 Sort users based on their seniority level 
// //    for Designation - Senior Developer > Developer > Intern
// //    for Age - 20 > 10



// Q4 Find all users with masters Degree.

// console.log("..........................Test4..........................")


const usersWithMasterDegree = Object.keys(users).filter(user => users[user].qualification == "Masters");
console.log(usersWithMasterDegree)





// Q5 Group users based on their Programming language mentioned in their designation.